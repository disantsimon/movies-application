import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {MovieListComponent} from "../../pages/movie-list/movie-list.component";
import {MovieDetailComponent} from "../../pages/movie-detail/movie-detail.component";
import {NotFoundComponent} from "../../pages/not-found/not-found.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'movies',
    pathMatch: 'full'
  },
  {
    path: 'movies',
    component: MovieListComponent
  },
  {
    path: 'movies/:id',
    component: MovieDetailComponent
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouterModule {
}
