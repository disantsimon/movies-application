import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './slider/slider.component';
import { HeaderComponent } from './header/header.component';
import {FormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterModule} from "@angular/router";
import { ImageNotAvailableComponent } from './image-not-available/image-not-available.component';


@NgModule({
  declarations: [SliderComponent, HeaderComponent, ImageNotAvailableComponent],
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot(),
    RouterModule
  ],
  exports: [SliderComponent, HeaderComponent, ImageNotAvailableComponent]
})
export class ComponentsModule { }
