import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageNotAvailableComponent } from './image-not-available.component';
import {TranslateModule} from "@ngx-translate/core";

describe('ImageNotAvailableComponent', () => {
  let component: ImageNotAvailableComponent;
  let fixture: ComponentFixture<ImageNotAvailableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageNotAvailableComponent ],
      imports: [TranslateModule.forRoot()],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageNotAvailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
