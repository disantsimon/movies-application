import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-image-not-available',
  templateUrl: './image-not-available.component.html',
  styleUrls: ['./image-not-available.component.scss']
})
export class ImageNotAvailableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
