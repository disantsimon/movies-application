import {Component, ElementRef, HostListener, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IMovie} from "../../../models/movie.model";
import {Subject, Subscription} from "rxjs";
import {debounceTime} from "rxjs/operators";


@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit, OnDestroy {
  @Input() movies: IMovie[];
  @Input() genre: any;

  errors: { [id: number]: boolean } = {};

  @ViewChild('slides') slides: ElementRef;
  private slideDelta = 0;
  private _slided$ = new Subject();
  private slided$ = this._slided$.asObservable().pipe(debounceTime(100));
  private slidedSubscription: Subscription;


  constructor() {
  }


  @HostListener('wheel', ['$event'])
  wheel($event: WheelEvent) {
    const el = this.slides.nativeElement;
    const isOverflowing = el.clientWidth < el.scrollWidth
      || el.clientHeight < el.scrollHeight;

    const canScroll = $event.deltaY > 0 ? (el.scrollLeft !== (el.scrollWidth - el.offsetWidth)) : (el.scrollLeft > 0);

    if(!canScroll){
      return;
    }

    if(isOverflowing){
      $event.preventDefault();
    }

    if ($event.deltaY >= 0) {
      this.slideDelta = this.slideDelta < 0 ? 0 : this.slideDelta;
      this.slideDelta += $event.deltaY;
    } else {
      if (this.slideDelta > 0) {
        this.slideDelta = 0
      }
      this.slideDelta += $event.deltaY;
    }
    this._slided$.next($event.deltaY);
  }


  ngOnInit(): void {
    this.slidedSubscription = this.slided$.subscribe(value => {
      this.slides.nativeElement.scrollLeft += this.slideDelta;
      this.slideDelta = 0;
    })
  }


  error($event: Event, movie: IMovie) {
    this.errors[movie.id] = true;
  }

  ngOnDestroy(): void {
    this.slidedSubscription.unsubscribe();
  }
}
