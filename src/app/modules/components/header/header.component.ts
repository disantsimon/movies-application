import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject, Subscription} from "rxjs";
import {debounceTime} from "rxjs/operators";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Input() hideSearch = false;
  @Input() canGoBack = false;

  _search$: Subject<string> = new Subject();

  @Output() search: EventEmitter<string> = new EventEmitter<string>();
  searchText: string;
  private searchSubscription: Subscription;

  @Output() backPressed: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
    this.searchSubscription = this._search$.pipe(
      debounceTime(450)
    ).subscribe(
      value => this.search.emit(value.trim())
    )
  }

  ngOnDestroy(): void {
    this.searchSubscription.unsubscribe();
  }
}
