import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const customHeader = {
      token: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
    };

    const newReq = req.clone({setHeaders: customHeader});

    return next.handle(newReq).pipe(
      catchError((err: HttpErrorResponse) => {
        this.handleError(err)
        return throwError(err)
      })
    );
  }

  handleError(err: HttpErrorResponse) {
    switch (err.status) {

      case 400:
        break;
      case 401:
        break;
      case 403:
        break;
      case 404:
        break;

        // etc...
    }
  }
}
