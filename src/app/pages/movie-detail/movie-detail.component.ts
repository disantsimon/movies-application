import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MovieService} from "../../services/movie.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription, throwError} from "rxjs";
import {IMovie} from "../../models/movie.model";
import {catchError, switchMap, tap} from "rxjs/operators";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {
  @ViewChild('main', {static: false}) main: ElementRef;
  private params$: Subscription;
  movie: IMovie;
  relatedMovies: IMovie[];
  mostRated: IMovie[];
  errorImage: boolean;

  constructor(private movieService: MovieService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private translate:TranslateService) {
  }

  ngOnInit(): void {
    this.params$ = this.activatedRoute.params.subscribe(
      params => {
        const id = parseInt(params.id);
        this.movieService.getMovie(id)
          .pipe(
            tap(movie => this.movie = movie),
            switchMap(
              movie => this.movieService.getRelatedMovies(movie)
            ),
            catchError(err => {
              if (err.code === 404) {
                this.back();
                alert(this.translate.instant('resourceNotFound'));
              }
              return throwError(err);
            })
          )
          .subscribe(
            movies => {
              this.relatedMovies = movies;
              this.movieService.mostRated(this.movie).subscribe(
                mostRated => this.mostRated = mostRated
              )

              this.main.nativeElement.scrollTop = 0;
            }
          )
      }
    )
  }

  back() {
    this.router.navigate(['movies'])
  }
}
