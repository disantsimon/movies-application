import {Component, OnInit} from '@angular/core';
import {MovieService} from "../../services/movie.service";
import {GenreType, IMovie} from "../../models/movie.model";

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {
  movies: IMovie[];
  moviesGroupedByGenre: { [genre: string]: IMovie[] };
  topRated: IMovie[] = [];
  genres = Object.keys(GenreType).map(key => GenreType[key]);

  constructor(private movieService: MovieService) {
  }

  ngOnInit(): void {
    this.movieService.getMovies().subscribe(
      movies => {
        this.movies = movies
        this.processMovies();
      }
    )
  }

  processMovies(filter: string = undefined) {
    this.moviesGroupedByGenre = {};
    const movies = this.movies.filter(movie => movie.name.toLowerCase().includes((filter || movie.name).toLowerCase()));
    this.topRated = movies.map(movie => movie).sort((m1, m2) => m2.rate - m1.rate);
    movies.forEach(
      movie => {
        movie.genres.forEach(genre => {
          const tmp = this.moviesGroupedByGenre[genre];
          this.moviesGroupedByGenre[genre] = tmp === undefined ? [movie] : tmp.concat(movie).sort((a, b) => b.rate - a.rate)
        })
      }
    )
  }

  filterFilms($event: string) {
    this.processMovies($event);
  }
}
