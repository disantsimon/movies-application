import {TestBed} from '@angular/core/testing';

import {MovieService} from './movie.service';
import {HttpClientModule} from "@angular/common/http";

describe('MovieService', () => {
  let service: MovieService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(MovieService);
  });

  it('should be created', async () => {
    expect(service).toBeTruthy();
  });

  it('httpWork', async () => {
    const movies = await service.getMovies().toPromise()

    expect(Array.isArray(movies)).toBe(true, "Movies should be an array");

    try {
      const movie = await service.getMovie(null).toPromise();

      expect(Array.isArray(movie)).toBe(null, "Movies should be null");
    } catch (e) {
      expect(e.message).toBe("film_id null was not found")
    }

    console.log(movies[0].id)

    const found = await service.getMovie(movies[0].id).toPromise();

    expect(found.id).toBe(movies[0].id, "Movies should be null");
  })
});
