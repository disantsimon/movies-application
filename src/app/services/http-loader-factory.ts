import {HttpClient} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {ConfigService} from "./config.service";

export function createTranslateLoader(http: HttpClient, config: ConfigService) {
  return new TranslateHttpLoader(http, config.i18n + '/i18n.', '.json');
}
