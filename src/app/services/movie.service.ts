import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";
import {Observable} from "rxjs";
import {IMovie} from "../models/movie.model";
import {map} from "rxjs/operators";
import {NotFoundException} from "../models/notFoundException";

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private paths = {
    movies: '/movie.mock-data.json'
  }

  constructor(private httpService: HttpClient, private configService: ConfigService) {
  }

  getMovies(): Observable<IMovie[]> {
    return this.httpService.get<IMovie[]>(this.configService.base_path + this.paths.movies).pipe(
      map(items => {
        items.forEach(movie => {
          movie.img = this.configService.media_path + '/' + movie.img;
          movie.rate = parseFloat(movie.rate?.toString())
        })
        return items;
      })
    );
  }

  getMovie(id: number): Observable<IMovie> {
    /**
     * this is just for simulating a call to endpoint "movies/:id"
     */
    return this.getMovies().pipe(
      map(movies => {
        const movie = movies.find(movie => movie.id === id)
        if (movie === undefined) {
          throw new NotFoundException(`film_id ${id} was not found`);
        }
        return movie;
      })
    );
  }

  getRelatedMovies(movie: IMovie): Observable<IMovie[]> {
    return this.getMovies().pipe(
      map(movies => {
        return movies.filter($movie => $movie.id !== movie.id && $movie.genres
          .find(genre => movie.genres.indexOf(genre) > -1))
          .sort((a, b) => b.rate - a.rate)
      })
    )
  }

  mostRated(movie: IMovie): Observable<IMovie[]> {
    return this.getMovies().pipe(
      map(movies => {
        return movies.filter($movie => $movie.rate > movie.rate).sort((a, b) => b.rate - a.rate).slice(0, 10)
      })
    )
  }

}
