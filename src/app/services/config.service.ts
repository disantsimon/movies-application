import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  base_path;
  media_path: string;
  i18n: string;

  constructor() {
    this.base_path = environment.base_path;
    this.media_path = environment.media_path;
    this.i18n = environment.i18n;
  }


}
