
export class NotFoundException extends Error{

  code = 404;

  constructor(message: string) {
    super();
    this.message = message;
    this.name = 'NOT_FOUND'
  }

}
