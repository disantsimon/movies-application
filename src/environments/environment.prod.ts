export const environment = {
  production: true,
  base_path: 'assets/mock-server',
  media_path: 'assets/images/movie-covers',
  i18n: 'assets/i18n'
};
